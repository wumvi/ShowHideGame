'use strict';
/* global ShowHide, CanvasEngine, EventDomDispatcher, EventEmitter */

/**
 *
 * @const {number}
 */
const COUNT_OF_OPEN_TITLE_DEFAULT_VALUE = 0;

/**
 * Время отображения квадратов открытыми
 * @const {number}
 */
const SHOW_OPEN_TILE_TIME = 1500;

/**
 * @export
 * @const
 */
const SHOW_HIDE_EVENT = {
    DEMON_SHOW: 'showhide-demon-show', // На показа демона
    TILE_CLICK: 'showhide-tile-click', // Клик по ячейке
    END_GAME: 'showhide-end-game', // Конец игры
    TILE_OPEN_PAIR: 'showhide-tile-open-pair' // Открыта правильно пара
}

/**
 * @export
 */
class ShowHideGameController {

    /**
     * @param {ShowHideMapBase} map
     * @param {HTMLTableElement} tableBox
     * @param {EventEmitter} eventManager
     * @param {string=} eventPrefix
     */
    constructor(map, tableBox, eventPrefix = '') {
        /**
         *
         * @type {HTMLTableElement}
         * @private
         */
        this.tableBox = tableBox;

        if (!(this.tableBox instanceof HTMLTableElement)) {
            throw new Error('Element obj not is HTMLTableElement');
        }

        /**
         *
         * @type {EventEmitter}
         */
        this.eventManager = EventManager.getMe();

        /**
         *
         * @type {ShowHideMapBase}
         * @private
         */
        this.map = map;

        /**
         * Устанавливаем паузу в игре
         * @private
         * @type {boolean}
         */
        this.isGameEnd = false;

        /**
         * Количество правильно открытых пар-карт
         * @type {number}
         * @private
         */
        this.countOfOpenedTile = COUNT_OF_OPEN_TITLE_DEFAULT_VALUE;

        /**
         *
         * @type {!jQuery}
         * @private
         */
        this.$root = jQuery(this.tableBox);

        /**
         *
         * @type {boolean}
         * @private
         */
        this.isCanClick = !this.map.isLock();

        /**
         * Массив моделей слов, с которыми идёт взаимодействие, т.е. открывающиеся
         * @type {Array.<ShowHideWordModel>}
         * @private
         */
        this.openWordList = [];

        /**
         *
         * @type {ShowHideDemonModeInterface}
         * @private
         */
        this.demonCurrent = null;

        /**
         *
         * @type {Array.<ShowHideTile>}
         * @private
         */
        this.tileList = [];

        /**
         * Префикс для событий
         * @type {string}
         * @private
         */
        this.eventPrefix = eventPrefix;

        /**
         * @private
         * @type {?UserTutorialController}
         */
        this.userTutorial = null;

        this.init();
    }

    /**
     * Вылечиваем все паразитированные ячейки
     * @export
     */
    cureTile() {
        // for (let num = 0; num < this.tileDrawList.length; num += 1) {
        //     this.tileDrawList[num].setDemonLogic(null);
        // }
    }

    /**
     * @param {boolean} flag
     */
    setCanClickFlag(flag) {
        this.isCanClick = flag;
    }

    /**
     * Передаём управление демону, чтобы он мог начать паразитировать
     * @export
     */
    parasiteTile() {
        this.demonCurrent.reset();
        this.demonCurrent.start();
    }

    /**
     * Показывать ли подсказки
     * @export
     */
    showTip() {
        this.setCanClickFlag(false);
        this.setTipFlag(true);
        setTimeout(this.onShowTipTimeout.bind(this), 3000);
    }

    /**
     *
     * @private
     */
    onShowTipTimeout() {
        this.showTipFlag(false);
        this.setCanClickFlag(true);
        this.demonCurrent.setUsedDemonMode();
    }

    /**
     * @param {boolean} flag
     * @private
     */
    setTipFlag(flag) {
        // for (let num = 0; num < this.tileDrawList.length; num += 1) {
        //     if (!this.tileDrawList[num].isCanShowTipMode()) {
        //         continue;
        //     }
        //
        //     this.tileDrawList[num].setShowTipFlag(flag);
        // }
    }

    /**
     *
     * @private
     */
    init() {
        this.createTableInner();
        this.initEvent();
    }

    /**
     * Создаём внутреннюю структуру таблицы
     * @private
     */
    createTableInner() {
        let className;
        for (let colNum = 0; colNum < this.map.getCol(); colNum += 1) {
            let $trElement = jQuery(document.createElement('tr'));
            className = 'shg-row shg-row--num' + colNum + ' shg-row-num' + colNum + '__js';
            $trElement.addClass(className);
            for (let rowNum = 0; rowNum < this.map.getRow(); rowNum += 1) {
                let $tdElement = jQuery(document.createElement('td'));
                let colorRnd = _.random(1, 9);
                let tileNum = this.tileList.push(new ShowHideTile($tdElement)) - 1;
                className = 'shg-tile shg-tile--color' + colorRnd;
                className += ' shg-tile--num' + tileNum + ' shg-tile-num' + tileNum + '__js';
                className += ' shg-tile--col' + rowNum + ' shg-tile-col' + rowNum + '__js ';
                $tdElement.addClass(className);
                $tdElement.attr('data-num', tileNum);

                $trElement.append($tdElement);
            }

            this.$root.append($trElement);
        }

        this.$root.addClass('shg-table--row' + this.map.getRow());
        setTimeout(this.onWindowResize.bind(this), 10);
    }

    /**
     * @private
     */
    onWindowResize() {
        this.tileList.map(tileItem => {
            tileItem.resize();
        });
    }

    active(){
        setTimeout(this.onWindowResize.bind(this), 10);
    }

    /**
     *
     * @private
     */
    initEvent() {
        this.$root.on('click touchstart', this.onRootTouchStart.bind(this));
        jQuery(window).resize(this.onWindowResize.bind(this));
    }

    /**
     *
     * @param {jQuery.Event} event
     * @return {boolean}
     * @private
     */
    onRootTouchStart(event) {
        let $cell = jQuery(event.target);
        let cellNum = /** @type {number} */ ($cell.data('num'));
        if (typeof cellNum === 'undefined') {
            return false;
        }

        this.onTileClick(cellNum);
        return false;
    }

    /**
     *
     * @param {string} demonKey
     * @return {?ShowHide.DemonModeInterface}
     * @private
     */
    makeDemonLogic(demonKey) {
        // switch (demonKey) {
        //     case ShowHide.Demon.Ouroboros.ID_KEY:
        //         let res = new ShowHide.Demon.OuroborosRes(this.loaderData[ShowHide.Demon.Ouroboros.ID_KEY]);
        //         return new ShowHide.Demon.Ouroboros(res, this);
        // }

        return null;
    }

    /**
     *
     * @param {number} tileNum
     */
    openTile(tileNum) {
        // Получаем модель Ячейки
        let tileItem = this.tileList[tileNum];

        // Модель слова в ячейке
        let wordInfo = tileItem.getWordInfo();

        this.eventManager.emitEvent(
            SHOW_HIDE_EVENT.TILE_CLICK + this.eventPrefix,
            [new ShowHideTileClickEvent(wordInfo), tileNum]
        );

        // Если модель слова не задана, то нужно его получить
        if (wordInfo === null) {
            wordInfo = this.map.getNextWordInfo();
            tileItem.setWordInfo(wordInfo);
            this.map.callCallbackTileDrawSetCallback(tileItem);
            // Если есть демон, то задаём его логику
            wordInfo.setDemonLogic(this.makeDemonLogic(wordInfo.getId()));
        }

        // Если в ячейки хранится демон, то нужно выполнить его логику
        if (wordInfo.getDemonId()) {
            // Закрываем другие ячейки
            this.initTimerToCloseWrongTiles();
            // Запрещаем взаимодействие с доской
            this.setCanClickFlag(false);

            // Получаем логику демона
            this.demonCurrent = wordInfo.getDemonLogic();

            // Посылаем сообщение в эфир, что демон открылся
            this.eventManager.emitEvent(
                SHOW_HIDE_EVENT.DEMON_SHOW + this.eventPrefix,
                [new ShowHideDemonEvent(wordInfo)]
            );
            return;
        }

        tileItem.open();

        if (this.wrongTileOpenHandle) {
            clearTimeout(this.wrongTileOpenHandle);
            this.wrongTileOpenHandle = null;
            this.closeWrongTiles_();
        }

        // Добавляем открытую ячейки в список открытых
        this.openWordList.push(wordInfo);

        // Если мы открыли достаточно ячеек для проверки
        if (this.openWordList.length === this.map.getAnswerCount()) {
            // Проверяем правильный ли ответ содержится в ячейках
            if (this.isRightTileAnswer()) {
                this.wasOpenRightTile();
            } else {
                this.wasOpenWrongTile();
            }
        }
    }

    /**
     * Обработка клика по полю
     * @param {number} tileNum Номер ячейки
     * @private
     */
    onTileClick(tileNum) {
        // Получаем модель Ячейки
        let tileItem = this.tileList[tileNum];

        if (this.isGameEnd) {
            return;
        }

        if (this.userTutorial && this.userTutorial.getCurrentItem().callLogicCallback(this, this.userTutorial, tileNum)) {
            this.openTile(tileNum);
            return;
        }

        /**
         * Если ячейка уже открыта
         * или кликать клики запрещены
         * или стоит пауза
         * то нельзя кликать
         */
        if (tileItem.isOpen() || !this.isCanClick) {
            return;
        }

        this.openTile(tileNum);
    }

    /**
     *
     * @private
     */
    wasOpenRightTile() {
        this.openWordList.map(openWord => {
            openWord.getTileItem().markAsRight();
        })

        setTimeout(this.unMarkRightTiles.bind(this, this.openWordList), 1000);

        this.wordOpenListFree();
        this.setCanClickFlag(true);
        this.countOfOpenedTile += 1;

        if (this.countOfOpenedTile === this.map.getTitlePairCount()) {
            this.eventManager.emit(SHOW_HIDE_EVENT.END_GAME + this.eventPrefix);
            this.isGameEnd = true;
        } else {
            this.eventManager.emitEvent(
                SHOW_HIDE_EVENT.TILE_OPEN_PAIR + this.eventPrefix,
                [new ShowHideOpenPairEvent(this.countOfOpenedTile)]
            );
        }
    }

    /**
     *
     * @param {Array.<ShowHideWordModel>} openWordList
     */
    unMarkRightTiles(openWordList) {
        openWordList.map(openWord => {
            openWord.getTileItem().unMarkAsRight();
        });
    }

    /**
     *
     * @private
     */
    wasOpenWrongTile() {
        this.openWordList.map(openWord => {
            openWord.getTileItem().markAsWrong();
        });

        this.initTimerToCloseWrongTiles();
    }

    /**
     *
     * @private
     */
    wordOpenListFree() {
        this.openWordList = [];
    }

    /**
     * @private
     */
    initTimerToCloseWrongTiles() {
        this.wrongTileOpenHandle = setTimeout(() => {
            this.closeWrongTiles_();
        }, SHOW_OPEN_TILE_TIME);
    }

    /**
     *
     * @private
     */
    closeWrongTiles_() {
        this.openWordList.map(openWord => {
            openWord.getTileItem().close();
        });

        this.wordOpenListFree();
    }

    /**
     *
     * @return {boolean}
     * @private
     */
    isRightTileAnswer() {
        let result = this.map.callCheckRightAnswerCallback(this.openWordList);
        if (result !== null) {
            return result;
        }

        let tileId = this.openWordList[0].getId();
        let flag = true;
        for (let num = 1; num < this.openWordList.length; num += 1) {
            if (this.openWordList[num].getId() !== tileId) {
                flag = false;
                break;
            }
        }

        return flag;
    }


    /**
     * @return {number}
     * @export
     */
    getWidth() {
        return this.$root.width();
    }

    /**
     * @export
     */
    resetGame() {
        this.map.reset();
        this.isGameEnd = false;

        this.tileList.map(tileItem => {
            tileItem.close();
            tileItem.setWordInfo(null);
        });

        this.openWordList = [];
        this.countOfOpenedTile = COUNT_OF_OPEN_TITLE_DEFAULT_VALUE;
    }

    /**
     *
     * @param {UserTutorialController} userTutorial
     */
    setUserTutorial(userTutorial) {
        this.userTutorial = userTutorial;
    }
}
