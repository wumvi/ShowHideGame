'use strict';
/** @define {boolean} */
let ENABLE_QUNIT_TEST = true;


/**
 *
 * @export
 */
class ShowHide {
    /**
     * Тип сообщений от игры
     * @const {string}
     * @export
     */
    // static get EVENT_NAME() {
    //     return 'ShowHide';
    // }

    /**
     * Показан демон
     * @const {string}
     * @export
     */
    // static get EVENT_DEMON_SHOW() {
    //     return 'ShowHide.Demon.Show'
    // }

    /**
     * Клик по плитке
     * @const {string}
     * @export
     */
    // static get EVENT_TILE_CLICK() {
    //     return 'ShowHide.Tile.Click';
    // }

    /**
     * Клик по плитке
     * @const {string}
     * @export
     */
    // static get EVENT_TILE_OPEN_PAIR() {
    //     return 'ShowHide.Tile.OpenPair';
    // }

    /**
     * Игра закончена
     * @const {string}
     * @export
     */
    // static get EVENT_END_GAME() {
    //     return 'ShowHide.Game.End';
    // }
}

/**
 *
 * @export
 */
ShowHide.Demon = class {

};

/**
 *
 * @type {Object}
 * @export
 */
ShowHide.Demon.Resource = {};

/**
 * @class
 */
ShowHide.Draw = class {
};
