// 'use strict';
// /* global ShowHide */
//
// /**
//  * Логика демона. Данные демон отвечает за перемешивание ранее открытых карт.
//  * @param {ShowHide.Demon.OuroborosRes} demonRes
//  * @param {ShowHideGameController} gameController
//  * @implements {ShowHideDemonModeInterface}
//  * @constructor
//  */
// ShowHide.Demon.Ouroboros = function (demonRes, gameController) {
//
//     /**
//      * Объект отвечающий за визуальную отрисовку
//      * @type {ShowHide.Draw.Ouroboros}
//      * @private
//      */
//     this.sprite_ = new ShowHide.Draw.Ouroboros(demonRes);
//
//     /**
//      * Хранит значение для обратного отсчёта
//      * @type {number}
//      * @private
//      */
//     this.countDown_ = -1;
//
//     /**
//      * Game Controller
//      * @type {ShowHideGameController}
//      * @private
//      */
//     this.gameController_ = gameController;
// };
//
//
// /**
//  * Включаем обратный отсчёт
//  */
// ShowHide.Demon.Ouroboros.prototype.start = function () {
//     this.gameController_.setIsCanClickFlag(true);
//     this.timerCountDown_();
// };
//
//
// /**
//  * Логика обратного отсчёта
//  * @private
//  */
// ShowHide.Demon.Ouroboros.prototype.timerCountDown_ = function () {
//     this.sprite_.setText(this.countDown_ + '');
//
//     setTimeout(() => {
//         this.countDown_ -= 1;
//
//         if (this.countDown_ === 0) {
//             this.stopAndDoBadThing_();
//             return;
//         }
//
//         this.sprite_.setText(this.countDown_ + '');
//         this.timerCountDown_();
//     }, 1000);
// };
//
//
// /**
//  * Останавливаем обратный отчёт и мешает карточки
//  * @private
//  */
// ShowHide.Demon.Ouroboros.prototype.stopAndDoBadThing_ = function () {
//     this.gameController_.cureTile();
//     this.setUsedDemonMode();
//     this.shuffleList_();
// };
//
//
// /**
//  * Функция для сортивки. Был ли квадрат ранее открытым
//  * @param {ShowHide.Draw.Tile} tileDraw
//  * @private
//  * @return {boolean}
//  */
// ShowHide.Demon.Ouroboros.prototype.filterCanShuffle_ = function (tileDraw) {
//     let wordInfo = tileDraw.getWordInfo();
//     return wordInfo !== null && wordInfo.isWasOpened() && wordInfo.getDemonId() === null && !tileDraw.isOpen();
// };
//
//
// /**
//  *
//  * @private
//  */
// ShowHide.Demon.Ouroboros.prototype.shuffleList_ = function () {
//     let openedTileList = this.gameController_.getTileList().filter(ShowHide.Demon.Ouroboros.prototype.filterCanShuffle_);
//
//     let wordInfoList = openedTileList.map(function (tileDraw) {
//         return tileDraw.getWordInfo();
//     });
//
//     openedTileList = openedTileList.shuffle();
//
//     for (let num = 0; num < openedTileList.length; num += 1) {
//         openedTileList[num].setWordInfo(wordInfoList[num]);
//     }
// };
//
//
// /**
//  * Указываем, что демон отжил своё и надо рисовать его образ
//  */
// ShowHide.Demon.Ouroboros.prototype.setUsedDemonMode = function () {
//     this.sprite_.setUsedDemonMode();
// };
//
//
// /**
//  * Обнуляем счётчик
//  */
// ShowHide.Demon.Ouroboros.prototype.reset = function () {
//     this.countDown_ = 10;
// };
//
//
// /**
//  * Получаем объек отвечающий за отрисовку
//  * @return {ShowHide.DemonDrawInterface}
//  */
// ShowHide.Demon.Ouroboros.prototype.getSprite = function () {
//     return this.sprite_;
// };
//
//
// /**
//  * ID демона
//  * @const {string}
//  */
// ShowHide.Demon.Ouroboros.ID_KEY = 'demon.ouroboros';
//
//
// /**
//  * Получаем ID демона
//  * @return {string}
//  */
// ShowHide.Demon.Ouroboros.prototype.getId = function () {
//     return ShowHide.Demon.Ouroboros.ID_KEY;
// };
//
//
// /**
//  * Ресурсы для загрузки
//  *
//  * @type {{body: string, light: string, top: string}}
//  */
// ShowHide.Demon.Resource[ShowHide.Demon.Ouroboros.ID_KEY] = {
//     'body': 'res/img/ouruboros/%size%/%scaled%body-open.jpg',
//     'light': 'res/img/ouruboros/%size%/%scaled%body-close.jpg',
//     'top': 'res/img/ouruboros/%size%/%scaled%parasite.jpg'
// };
