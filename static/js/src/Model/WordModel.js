'use strict';
/* global ShowHide */


/**
 *
 */
class ShowHideWordModel {
    /**
     * @param {string} id
     * @param {?string} word
     * @param {?string} demonId
     * @param {Object=} userData
     */
    constructor(id, word, demonId, userData = {}) {
        /**
         *
         * @type {string}
         * @private
         */
        this.id = id;

        /**
         *
         * @type {?string}
         * @private
         */
        this.wordText = word;

        /**
         *
         * @type {?string}
         * @private
         */
        this.demonId = demonId;

        /**
         *
         * @type {?ShowHideTile}
         * @private
         */
        this.tileItem = null;

        /**
         *
         * @type {?ShowHideDemonModeInterface}
         * @private
         */
        this.demonLogic = null;

        /**
         * Была ли хоть раз открыта ячейка
         * @type {boolean}
         * @private
         */
        this.isWasOpened = false;

        /**
         * @type {Object}
         * @private
         */
        this.userData = userData;
    }

    /**
     * Уставливаем, что слово открывалось хоть раз
     * @param {boolean} flag
     */
    setOpenFlag(flag) {
        this.isWasOpened = flag;
    }

    /**
     * Открывался ли квадрат хоть один раз
     * @return {boolean}
     */
    isWasOpened() {
        return this.isWasOpened;
    }

    /**
     * Получаем пользовательские данные по ячейки
     * @return {Object}
     */
    getUserData() {
        return this.userData;
    }

    /**
     *
     * @param {ShowHideTile} tileItem
     */
    setTileItem(tileItem) {
        this.tileItem = tileItem;
    }

    /**
     *
     * @return {ShowHideTile}
     */
    getTileItem() {
        return this.tileItem;
    }

    /**
     *
     * @param {ShowHideDemonModeInterface} demonLogic
     */
    setDemonLogic(demonLogic) {
        this.demonLogic = demonLogic;
    }

    /**
     *
     * @return {ShowHideDemonModeInterface}
     */
    getDemonLogic() {
        return this.demonLogic;
    }

    /**
     *
     * @return {string}
     */
    getId() {
        return this.id;
    }

    /**
     *
     * @return {?string}
     */
    getWordText() {
        return this.wordText;
    }

    /**
     *
     * @return {?string}
     */
    getDemonId() {
        return this.demonId;
    }
}
