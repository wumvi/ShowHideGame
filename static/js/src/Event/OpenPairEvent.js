'use strict';
/* global ShowHide */


/**
 *
 */
class ShowHideOpenPairEvent {
    /**
     * @param {number} count
     * @constructor
     */
    constructor(count) {
        /**
         *
         * @type {number}
         * @private
         */
        this.count_ = count;
    }

    /**
     *
     * @return {number}
     */
    getCount() {
        return this.count_;
    }
}
