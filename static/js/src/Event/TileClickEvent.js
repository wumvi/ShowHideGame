'use strict';
/* global ShowHide */


/**
 *
 */
class ShowHideTileClickEvent {
    /**
     * @param {ShowHideWordModel} wordInfo
     */
    constructor(wordInfo) {
        this.wordInfo = wordInfo;
    }

    /**
     *
     * @return {ShowHideWordModel}
     */
    getWordInfo() {
        return this.wordInfo;
    }
}
