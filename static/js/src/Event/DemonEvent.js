'use strict';
/* global ShowHide */


/**
 *
 */
class ShowHideDemonEvent {
    /**
     * @param {ShowHideWordModel} wordInfo
     */
    constructor(wordInfo) {
        this.wordInfo = wordInfo;
    }

    /**
     *
     * @return {ShowHideWordModel}
     */
    getWordInfo() {
        return this.wordInfo;
    }
}
