// 'use strict';
// /* global ShowHide */
//
//
//
// /**
//  * Отрисовка всех состояния демона
//  *
//  * @param {ShowHide.Demon.OuroborosRes} demonRes
//  * @constructor
//  * @implements {ShowHide.DemonDrawInterface}
//  */
// ShowHide.Draw.Ouroboros = function(demonRes) {
//
//   /**
//    * Модель с изображениями демона
//    *
//    * @type {ShowHide.Demon.OuroborosRes}
//    * @private
//    */
//   this.demonRes_ = demonRes;
//
//   /**
//    * Рисовать образ или основное изображение
//    * @type {boolean}
//    * @private
//    */
//   this.isDemonCloseMode_ = false;
//
//   /**
//    * Текст, который пишем сверху квадрата
//    *
//    * @type {string}
//    * @private
//    */
//   this.text_ = '';
// };
//
//
// /**
//  * Отрисовываем образ демона или его основное изображение
//  *
//  * @param {CanvasEngine.Coordinates} coordinates
//  * @param {CanvasRenderingContext2D} ctx
//  * @param {number} timestamp
//  */
// ShowHide.Draw.Ouroboros.prototype.drawDemon = function(coordinates, ctx, timestamp) {
//   if (this.isDemonCloseMode_) {
//     ctx.drawImage(this.demonRes_.getLightImg(), coordinates.getX(), coordinates.getY());
//   } else {
//     ctx.drawImage(this.demonRes_.getBodyImg(), coordinates.getX(), coordinates.getY());
//   }
// };
//
//
// /**
//  * Указываем, что демон отжил своё и надо рисовать его образ
//  */
// ShowHide.Draw.Ouroboros.prototype.setUsedDemonMode = function() {
//   this.isDemonCloseMode_ = true;
// };
//
//
// /**
//  * Отрисовка сверху квадрата текста и фонового изображения
//  *
//  * @param {CanvasRenderingContext2D} ctx
//  * @param {number} timestamp
//  * @param {ShowHide.Draw.Tile} tileDraw
//  */
// ShowHide.Draw.Ouroboros.prototype.topParasiteDraw = function(ctx, timestamp, tileDraw) {
//   let tileWidth = tileDraw.getWidth();
//   let tileHeight = tileDraw.getHeight();
//
//   ctx.drawImage(this.demonRes_.getParasiteImg(), 0, 0);
//
//   let centerX = Math.floor(tileWidth / 2);
//   let centerY = Math.floor(tileHeight / 2);
//
//   ctx.fillStyle = '#ffffff';
//   ctx.beginPath();
//   ctx.fillText('' + this.text_, centerX, centerY);
//   ctx.fill();
// };
//
//
// /**
//  * Устанавливаем какой текст будем выводить сверху квадрата
//  *
//  * @param {string} text
//  */
// ShowHide.Draw.Ouroboros.prototype.setText = function(text) {
//   this.text_ = text;
// };
