'use strict';
/* global ShowHide, CanvasEngine, FUtils */

/**
 * Класс, что ячейка открыта
 * @const {string}
 */
const TILE_OPEN_CLASS = 'shg-tile--open';

/**
 * Класс, что в ячейке неверный ответ
 * @const {string}
 */
const TILE_WRONG_ANSWER_CLASS = 'shg-tile--wrong-answer';

/**
 * Класс, что в ячейке верный ответ
 * @const {string}
 */
const TILE_RIGHT_ANSWER_CLASS = 'shg-tile--right-answer';

/**
 * Медель квадрата
 */
class ShowHideTile {

    /**
     * @param {jQuery} $root
     */
    constructor($root) {
        /**
         * Логика демона
         * @type {?ShowHide.DemonModeInterface}
         * @private
         */
        this.demonLogic = null;

        /**
         * Настройки квадрата
         * @type {?ShowHideWordModel}
         * @private
         */
        this.wordInfo = null;

        /**
         * Текущее состояние ячейки: открыто или закрыто
         * @type {boolean}
         * @private
         */
        this.isOpen_ = false;

        /**
         * Root object
         * @type {jQuery}
         */
        this.$root = $root;
    }

    /**
     * Пометка что ответ указали правильно
     */
    markAsRight() {
        this.$root.addClass(TILE_RIGHT_ANSWER_CLASS);
    }

    unMarkAsRight() {
        this.$root.removeClass(TILE_RIGHT_ANSWER_CLASS);
    }

    /**
     * Устанавливаем логику демона
     * @param {ShowHide.DemonModeInterface} demonLogic
     */
    setDemonLogic(demonLogic) {
        this.demonLogic = demonLogic;
    }

    /**
     * Устанавливаем настройки по слову
     *
     * @param {?ShowHideWordModel} wordInfo
     */
    setWordInfo(wordInfo) {
        this.wordInfo = wordInfo;
        if (this.wordInfo) {
            this.wordInfo.setTileItem(this);
        }
    }

    /**
     * Получаем модель слова, если оно есть
     * @return {?ShowHideWordModel}
     */
    getWordInfo() {
        return this.wordInfo;
    }

    /**
     * Устанавливаем, что этот квадрат, должен показать подсказку
     * @param {boolean} isTipMode
     */
    setShowTipFlag(isTipMode) {
        this.tipMode_ = isTipMode;
        if (this.tipMode_) {
            // this.tipAlphaAnimation_.start();
        }

        console.log('tip flag');
    }

    /**
     * Можно ли показывать подсказку в этом квадрате
     * @return {boolean}
     */
    isCanShowTipMode() {
        return this.wordInfo !== null && this.wordInfo.isWasOpened() && this.wordInfo.getDemonId() === null;
    }

    /**
     * Можно ли данный квадрат паразитировать
     * @return {boolean}
     */
    isCanParasite() {
        return this.wordInfo !== null && this.wordInfo.isWasOpened() && this.wordInfo.getDemonId() === null;
    }

    /**
     * Открываем квадрат
     */
    open() {
        this.isOpen_ = true;
        this.wordInfo.setOpenFlag(true);
        this.$root.addClass(TILE_OPEN_CLASS);
        this.$root.html('<span class="sgh-text">' + this.wordInfo.getWordText() + '</span>');
    }

    /**
     * Отрыт ли квадрат?
     * @return {boolean}
     */
    isOpen() {
        return this.isOpen_;
    }

    /**
     * Пометка, что открыта не верная ячейка
     */
    markAsWrong() {
        this.$root.addClass(TILE_WRONG_ANSWER_CLASS);
    }

    /**
     * Закрываем квадрат
     */
    close() {
        this.isOpen_ = false;
        this.$root.removeClass(TILE_OPEN_CLASS);
        this.$root.removeClass(TILE_WRONG_ANSWER_CLASS);
    }

    resize(){
        let width = this.$root.width();
        this.$root.css({'height': width});
    }
}
