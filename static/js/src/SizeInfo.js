'use strict';
/* global ShowHide, ENABLE_QUNIT_TEST */

/**
 * @class
 */
class ShowHideSizeInfo {
    /**
     * Конструктор
     * @param {number} pixelRatio Размер пиксиля
     * @param {ShowHideMapBase} map Модель карты
     * @param {number} width Ширина игры
     */
    constructor(pixelRatio, map, width) {
        if (width === undefined) {
            throw new Error('Width is not set');
        }

        this.pixelRatio = pixelRatio;
        this.width = Math.floor((width - 20) / map.getCol());
    }

    /**
     * Получаем реальную ширину без расчёта pixelRatio
     * @return {number} Ширина
     */
    getRealWidth() {
        return this.width;
    }

    /**
     * Получаем ширину с учётом pixelRatio
     * @return {number} Ширина
     */
    getWidth() {
        return this.width * this.pixelRatio;
    }

    /**
     * Получаем размер пикселя
     * @return {number} Рамез пикселя
     */
    getPixelRatio() {
        return this.pixelRatio;
    }
}
