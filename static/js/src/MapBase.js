'use strict';
/* global ShowHide */

/**
 *
 * @const {number}
 */
const DEFAULT_KEY_ATTEMP_COUNT = 99;


/**
 *
 * @const {number}
 */
const DEFAULT_GAME_DURATION = 60 * 60;

/**
 * Значение по умолчанию для свойства this.nextFreeWordModel_
 * @const {number}
 */
const NEXT_FREE_WORD_MODEL_DEFAULT_VALUE = 0;

/**
 * Базовый класс карт
 * @constructor
 * @export
 */
class ShowHideMapBase {
    constructor() {
        /**
         * Массив моделей слов
         * @type {Array.<ShowHideWordModel>}
         * @private
         */
        this.wordList_ = [];

        /**
         * Количество колонок
         * @type {number}
         * @private
         */
        this.colCount_ = 0;

        /**
         * Количество строк
         * @type {number}
         * @private
         */
        this.rowCount_ = 0;

        /**
         * Количество ответов подряд
         * @type {number}
         * @private
         */
        this.answerCount_ = 2;

        /**
         * Какое слово будет идти по порядку. Инскриментится с каждым полученным словом
         * @type {number}
         * @private
         */
        this.nextFreeWordModel_ = NEXT_FREE_WORD_MODEL_DEFAULT_VALUE;


        /**
         * Какое количество пар нужно открыть, чтобы игра закончилась
         * @type {number}
         * @private
         */
        this.titlePairCount_ = 1;

        /**
         * Количество попыток на открытия карт
         * @type {number}
         * @private
         */
        this.keyAttemptCount_ = DEFAULT_KEY_ATTEMP_COUNT;

        /**
         * Продолжительность игры по времени
         * @type {number}
         * @private
         */
        this.gameDuration_ = DEFAULT_GAME_DURATION;

        /**
         * Флаг того, что теперь при достижении лимитов, игра закончится
         * @type {boolean}
         * @private
         */
        this.limitationFlag_ = false;

        /**
         *
         * @type {?Function}
         */
        this.cbCheckRightAnswer = null;

        /**
         *
         * @type {?Function}
         */
        this.cbCallbackTileDrawSet = null;

        /**
         *
         * @type {?Function}
         */
        // this.clickCallback = null;

        /**
         *
         * @type {boolean}
         */
        this.isLockFlag = false;
    }

    /**
     * Добавляем новое слово в список
     * @param {string} id id группы
     * @param {?string} word Текст слова
     * @param {?string} demonId ID демона
     * @param {Object=} userdata Пользовательские данные
     * @export
     */
    addWord(id, word, demonId, userdata) {
        this.wordList_.push(new ShowHideWordModel(id, word, demonId, userdata));
    }

    /**
     * Перемешиваем слова
     * @export
     */
    shuffleWordList() {
        this.wordList_ = _.shuffle(this.wordList_); // .shuffle().shuffle().shuffle().shuffle();
    }

    /**
     * Получаем слово по номер
     * @param {number} num Номер слова
     * @return {ShowHideWordModel}
     */
    getWord(num) {
        return this.wordList_[num];
    }

    /**
     * Выставляем флаг того, что теперь при достижении лимитов, игра закончится
     * @param {boolean} flag
     * @export
     */
    setLimitationFlag(flag) {
        this.limitationFlag_ = flag;
    }

    /**
     *
     * @return {boolean}
     */
    getLimitationFlag() {
        return this.limitationFlag_;
    }

    /**
     *
     * @param {?Function} cb
     */
    onTileDrawSetCallback(cb) {
        this.cbCallbackTileDrawSet = cb;
    }

    /**
     *
     * @param {ShowHideTile} tileItem
     */
    callCallbackTileDrawSetCallback(tileItem) {
        if (!this.cbCallbackTileDrawSet) {
            return;
        }

        this.cbCallbackTileDrawSet(tileItem);
    }

    /**
     *
     * @param {?Function} cb
     */
    onCheckRightAnswerCallback(cb) {
        this.cbCheckRightAnswer = cb;
    }

    /**
     *
     * @param {Array.<ShowHideWordModel>} tileList
     * @returns {?boolean}
     */
    callCheckRightAnswerCallback(tileList) {
        if (!this.cbCheckRightAnswer) {
            return null;
        }

        return this.cbCheckRightAnswer(tileList);
    }

    /**
     * Устанавливаем сколько должно быть парных ячеек
     * @param {number} numCount Сколько парных ячеек
     * @export
     */
    setAnswerCount(numCount) {
        this.answerCount_ = numCount;
    }

    /**
     * Устанавливаем, сколько выигрышных пар
     * @param {number} numCount Сколько выигрышных пар
     * @export
     */
    setTitlePairCount(numCount) {
        this.titlePairCount_ = numCount;
    }

    /**
     *
     * @return {number}
     */
    getTitlePairCount() {
        return this.titlePairCount_;
    }

    /**
     * Устанавливаем, количество попыток открытий
     * @param {number} num Количество попыток открытий
     * @export
     */
    setKeyAttemptCount(num) {
        this.keyAttemptCount_ = num;
    }

    /**
     * Получаем продолжительность игры
     * @return {number} Продолжительность игры в секундах
     */
    getGameDuration() {
        return this.gameDuration_;
    }

    /**
     * Устанавливаем продолжительность игры
     * @param {number} timeDuration Продолжительность игры в секундах
     * @export
     */
    setGameDuration(timeDuration) {
        this.gameDuration_ = timeDuration;
    }

    /**
     * Получаем количество попыток открытий
     * @return {number} Количество попыток открытий
     */
    getKeyAttemptCount() {
        return this.keyAttemptCount_;
    }

    /**
     * Получаем количество ответов для завершения игры
     * @return {number} Количество ответов для завершения
     */
    getAnswerCount() {
        return this.answerCount_;
    }

    /**
     * Устанавливаем количество строк и колонок
     * @param {number} rowCount Уставливаем количество строк
     * @param {number} colCount Устанавливаем количество колонок
     * @export
     */
    setRowAndCol(rowCount, colCount) {
        this.colCount_ = colCount;
        this.rowCount_ = rowCount;
    }

    /**
     * Получаем количество строк
     * @return {number} Количество строк
     */
    getRow() {
        return this.rowCount_;
    }

    /**
     * Получаем количество колонок
     * @return {number} Количество колонок
     */
    getCol() {
        return this.colCount_;
    }

    /**
     * Получаем список слов
     * @return {Array.<ShowHideWordModel>} Массив моделей слова
     */
    getWordInfoList() {
        return this.wordList_;
    }

    /**
     * Получаем следующее словао из списка
     * @return {ShowHideWordModel} Модель слова
     */
    getNextWordInfo() {
        let wordInfo = this.wordList_[this.nextFreeWordModel_];
        this.nextFreeWordModel_ += 1;
        return wordInfo;
    }

    /**
     * Перегрузка карты
     */
    reset() {
        this.shuffleWordList();
        this.nextFreeWordModel_ = NEXT_FREE_WORD_MODEL_DEFAULT_VALUE;
    }

    setLockFlag(flag) {
        this.isLockFlag = flag;
    }

    isLock() {
        return this.isLockFlag;
    }
    //
    // /**
    //  *
    //  * @param {Function} cb
    //  */
    // onClick(cb) {
    //     this.clickCallback = cb;
    // }
    //
    // /**
    //  *
    //  * @param {number} tileNum
    //  * @return {boolean}
    //  */
    // callClickCallback(){
    //     return this.clickCallback && this.clickCallback.apply(null, arguments);
    // }
}
