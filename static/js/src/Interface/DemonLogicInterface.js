'use strict';
/* global ShowHide */



/**
 *
 * @interface
 */
var ShowHideDemonModeInterface = function() {
};


/**
 *
 * @return {string}
 */
ShowHideDemonModeInterface.prototype.getId = function() {

};


/**
 *
 */
ShowHideDemonModeInterface.prototype.start = function() {

};


/**
 *
 */
ShowHideDemonModeInterface.prototype.reset = function() {

};


/**
 *
 * @return {ShowHideDemonDrawInterface}
 */
ShowHideDemonModeInterface.prototype.getSprite = function() {

};
