'use strict';
/* global ShowHide */



/**
 * @interface
 */
var ShowHideDemonDrawInterface = function() {
};


/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 * @param {ShowHideTile} tileItem
 */
ShowHideDemonDrawInterface.prototype.topParasiteDraw = function(ctx, timestamp, tileItem) {
};


/**
 * @param {CanvasEngine.Coordinates} coordinates
 * @param {CanvasRenderingContext2D} ctx
 * @param {number} timestamp
 */
ShowHideDemonDrawInterface.prototype.drawDemon = function(coordinates, ctx, timestamp) {

};


/**
 *
 */
ShowHideDemonDrawInterface.prototype.setUsedDemonMode = function() {

};
