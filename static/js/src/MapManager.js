/**
 *
 */
class ShowHideMapManager {
    constructor() {
        /**
         *
         * @type {Object.<string, ShowHideMapBase>}
         */
        this.mapList = {};
    }

    /**
     *
     * @param {string} id
     * @param {ShowHideMapBase} map
     */
    addMap(id, map) {
        this.mapList[id] = map;
    }

    /**
     *
     * @param {string} id
     * @returns {?ShowHideMapBase}
     */
    getMap(id) {
        return this.mapList[id] || null;
    }

    static getMe(){
        if (!window['showHideMapManager']) {
            window['showHideMapManager'] = new ShowHideMapManager();
        }

        return window['showHideMapManager'];
    }
}
