'use strict';
/* global ShowHide, ShowHideMapManager */

(function () {
    const TYPE_SUBJECT = 'sub';
    const TYPE_ADDITIION = 'add';

    /**
     *
     */
    class ShowHideMapUserData {
        /**
         *
         * @param {string} type
         * @param {?Array.<string>=} group
         * @constructor
         */
        constructor(type, group = []) {
            this.type_ = type;
            this.group_ = group;
        }

        /**
         *
         * @return {string}
         */
        getType() {
            return this.type_;
        }

        /**
         *
         * @return {?Array.<string>}
         */
        getGroup() {
            return this.group_;
        }
    }

    /**
     * @param {Array.<ShowHideWordModel>} tileList
     * @return {boolean}
     * @private
     */
    function cbCheckRightAnswerCustom(tileList)
    {
        let subjectTile = tileList[0];
        let additionTile = tileList[1];

        if (subjectTile.getUserData().getType() !== TYPE_SUBJECT) {
            let subjectTileTmp = subjectTile;
            subjectTile = additionTile;
            additionTile = subjectTileTmp;
        }

        let subjectTileGroup = subjectTile.getUserData().getGroup();

        return (subjectTile.getId() === additionTile.getId() || subjectTileGroup.indexOf(additionTile.getId()) !== -1) &&
            subjectTile.getUserData().getType() !== additionTile.getUserData().getType();
    }

    let map = new ShowHideMapBase();
    map.addWord('is', 'He', null, new ShowHideMapUserData(TYPE_SUBJECT, ['has']));
    map.addWord('has', 'has', null, new ShowHideMapUserData(TYPE_ADDITIION));

    map.addWord('is', 'is', null, new ShowHideMapUserData(TYPE_ADDITIION));
    map.addWord('has', 'He', null, new ShowHideMapUserData(TYPE_SUBJECT, ['is']));
    map.addWord('are', 'You', null, new ShowHideMapUserData(TYPE_SUBJECT, ['have']));
    map.addWord('are', 'are', null, new ShowHideMapUserData(TYPE_ADDITIION));
    map.addWord('has', 'Anna', null, new ShowHideMapUserData(TYPE_SUBJECT, ['is']));
    map.addWord('has', 'has', null, new ShowHideMapUserData(TYPE_ADDITIION));
    map.addWord('have', 'I', null, new ShowHideMapUserData(TYPE_SUBJECT, ['am']));
    map.addWord('have', 'have', null, new ShowHideMapUserData(TYPE_ADDITIION));
    map.addWord('am', 'I', null, new ShowHideMapUserData(TYPE_SUBJECT, ['have']));
    map.addWord('am', 'am', null, new ShowHideMapUserData(TYPE_ADDITIION));
    map.addWord('tr1', 'as', null, new ShowHideMapUserData(null));
    map.addWord('tr3', 'era', null, new ShowHideMapUserData(null));
    map.addWord('tr2', 'hos', null, new ShowHideMapUserData(null));
    map.addWord('tr4', 'hase', null, new ShowHideMapUserData(null));

    map.setThemeColor(MAP_GOLD_COLOR);

    map.setRowAndCol(4, 4);
    map.setAnswerCount(2);
    map.setTitlePairCount(6);
    // map.shuffleWordList();

    map.onCheckRightAnswerCallback(cbCheckRightAnswerCustom.bind(this));

    ShowHideMapManager.getMe().addMap('map1', map);
})();
