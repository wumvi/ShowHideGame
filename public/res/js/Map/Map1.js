'use strict';
/* global ShowHide */


/**
 * @extends {ShowHideMapBase}
 * @constructor
 */
class ShowHideMap1 extends ShowHideMapBase {
    constructor() {
        this.setRowAndCol(4, 4);
        this.addWord('freeze', 'Freeze', null);
        this.addWord('go', 'Went', null);
        this.addWord('ring', 'Ring', null);
        this.addWord(ShowHide.Demon.Ouroboros.ID_KEY, null, 'our1');
        this.addWord('deal', 'Dealt', null);
        this.addWord('ring', 'Rung', null);
        this.addWord('freeze', 'Frozen', null);
        this.addWord('go', 'Go', null);
        this.addWord('deal', 'Deal', null);
        this.addWord('think', 'Think', null);
        this.addWord('think', 'Thought', null);
        this.addWord(ShowHide.Demon.Ouroboros.ID_KEY, null, 'our2');
        this.setAnswerCount(2);
        this.setTitlePairCount(4);
    }
}
