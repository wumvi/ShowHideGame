<?php

namespace Index;

define('CORE_ROOT', 'lib/');

include 'vendor/autoload.php';
include 'autoloader.php';

$loader = new \Twig_Loader_Filesystem('tpl');

$twig = new \Twig_Environment($loader, array(
    // Uncomment the line below to cache compiled templates
    // 'cache' => __DIR__.'/../cache',
));

echo $twig->render('demo.twig', []);