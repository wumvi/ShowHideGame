'use strict';
/* global require */

let gulp = require('gulp');
let jsConcatDir = './public/res/js/concat/';

let concat = require('gulp-concat-util');
let yaml = require('js-yaml');
let fs = require('fs');
let removeUseStrict = require('gulp-remove-use-strict');
let merge = require('merge-stream');
var sass = require('gulp-sass');


let folderSassBin = 'public/res/css/';
let folderSassSrc = 'static/sass/';

gulp.task('concat-js', function () {
    let buildProperties = yaml.safeLoad(fs.readFileSync('static/js/build.yaml', 'utf8'));
    console.log(buildProperties);
    return gulp.src(buildProperties.list)
        .pipe(concat(buildProperties.name))
        .pipe(removeUseStrict())
        .pipe(gulp.dest(jsConcatDir));
});


/**
 *
 * @param {boolean} isDev
 * @param {string} type
 * @return {merge}
 */
function buildSass(isDev, type) {
    // let postcss      = require('gulp-postcss');
    // let sourcemaps   = require('gulp-sourcemaps');
    // let autoprefixer = require('autoprefixer');
    let replace = require('gulp-replace');

    let tasks = [];

    let versionData;
    versionData = {};

    versionData.css = (new Date()).getTime();

    let buildProperties = yaml.safeLoad(fs.readFileSync('static/sass/' + type + '/build.yaml', 'utf8'));
    for (let num in buildProperties.list) {
        if (!buildProperties.list.hasOwnProperty(num)) {
            continue;
        }

        let folderItem = buildProperties.list[num];
        let mainSassFile = folderSassSrc + type + '/' + folderItem + '/main.scss';

        let sassOptions = {};
        if (!isDev) {
            sassOptions.outputStyle = 'compressed';
        }

        let tempTask = gulp.src(mainSassFile)
            .pipe(sass({
                includePaths: [
                    './public/res/bower_package/breakpoint-sass/stylesheets'
                ]
            }))
            .pipe(sass(sassOptions).on('error', sass.logError))
            // for IE hack  http://keithclark.co.uk/articles/moving-ie-specific-css-into-media-blocks/media-tests/
            .pipe(replace('IEOnly', 'screen\\0'))
            .pipe(gulp.dest(folderSassBin + type + '/' + folderItem));
        tasks.push(tempTask);
    }

    // fs.writeFileSync(versionFile, JSON.stringify(versionData));

    return merge(tasks);
}


gulp.task('sass-custom-prod', buildSass.bind(null, false, 'prod'));