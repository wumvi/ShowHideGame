'use strict';
/* global CanvasEngine, goog */


'use strict';
/* global ShowHide */


/**
 * @extends {ShowHideMapBase}
 * @constructor
 */
function MyMapTest() {
    ShowHideMapBase.call(this);

    this.setRowAndCol(3, 4);

    this.addWord('right', 'Right 1', null);
    this.addWord('right', 'Right 2', null);
    this.addWord('wrong-1', 'Wrong 1', null);
    this.addWord('wrong-2', 'Wrong 2', null);

    this.setAnswerCount(2);
    this.setTitlePairCount(5);
    this.setThemeColor(ShowHideMapBase.GOLD_COLOR);

    this.onCallbackTileDrawSet = function(){
        throw new Error('onCallbackTileDrawSet test');
    };


    this.cbCheckRightAnswer = function(){
        throw new Error('cbCheckRightAnswer test');
    }
}

MyMapTest.prototype = Object.create(ShowHideMapBase.prototype);
MyMapTest.prototype.constructor = MyMapTest;
