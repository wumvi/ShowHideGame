'use strict';

/* global QUnit, CanvasEngine, EventDomDispatcher, MySprite */


/**
 *
 * @type {number}
 */
QUnit.config.testTimeout = 500;


QUnit.test('ShowHide.Draw.Tile', function (assert) {
    var canv =  /** @type {HTMLCanvasElement} */ (document.getElementById('canv'));
    var ctx = canv.getContext('2d');
    canv.width = 120;
    canv.height = 80;
    var gameCanvas = new CanvasEngine.GameCanvas(canv.width, canv.height);

    var tileDraw = new ShowHide.Draw.Tile(gameCanvas, 60, 10, 15);
    assert.ok(tileDraw instanceof ShowHide.Draw.Tile, 'object is ShowHide.Draw.Tile');
    assert.ok(tileDraw instanceof CanvasEngine.Sprite, 'object is CanvasEngine.Sprite');

    var coord = tileDraw.getCoordinates();
    assert.ok(coord instanceof CanvasEngine.Coordinates, 'coord is CanvasEngine.Coordinates');

    tileDraw.setTextColor('#696969');
    // tileDraw.setDemonLogic();
    // tileDraw.setWordInfo();
    // tileDraw.getWordInfo();
    tileDraw.setContextInfo(ctx);
    // tileDraw.setShowTipFlag();
    // tileDraw.isCanShowTipMode();
    // tileDraw.isCanParasite();
    // tileDraw.open();
    // tileDraw.isOpen();
    // tileDraw.close();


    console.log(coord);

    //tileDraw.draw(ctx, 0);

    // tileDraw.open(20, 20);
    //tileDraw.draw(ctx, 0);
});

QUnit.test('ShowHideMapBase', function (assert) {
    var mapBase = new MyMapTest();
    assert.ok(mapBase instanceof ShowHideMapBase, 'Constructor');

    var wordModel = mapBase.getWord(0);
    assert.ok(wordModel instanceof ShowHideWordModel, 'getWordText() return ShowHideWordModel');
    assert.ok(wordModel.getId() === 'right', 'getWordText() getId');
    assert.ok(wordModel.getWordText() === 'Right 1', 'getWordText() getName');
    assert.ok(wordModel.getDemonId() === null, 'getWordText() getDemonId');

    wordModel = mapBase.getWord(1);
    assert.ok(wordModel instanceof ShowHideWordModel, 'getWordText() return ShowHideWordModel');
    assert.ok(wordModel.getId() === 'right', 'getWordText() getId');
    assert.ok(wordModel.getWordText() === 'Right 2', 'getWordText() getName');
    assert.ok(wordModel.getDemonId() === null, 'getWordText() getDemonId');

    assert.ok(mapBase.getTitlePairCount() === 5, 'getTitlePairCount');
    assert.ok(mapBase.getAnswerCount() === 2, 'getAnswerCount');
    assert.ok(mapBase.getRow() === 3, 'getRow');
    assert.ok(mapBase.getCol() === 4, 'getCol');

    var wordList = mapBase.getWordInfoList();
    assert.ok(wordList.length === 4, 'getWordInfoList length');
    assert.ok(wordList[0] instanceof ShowHideWordModel, 'wordList instanceof ShowHideWordModel');

    wordModel = mapBase.getNextWordInfo();
    assert.ok(wordModel instanceof ShowHideWordModel, 'getNextWordInfo first call return ShowHideWordModel');
    assert.ok(wordModel.getId() === 'right', 'getNextWordInfo() getId');
    assert.ok(wordModel.getWordText() === 'Right 1', 'getNextWordInfo() getName');
    assert.ok(wordModel.getDemonId() === null, 'getNextWordInfo() getDemonId');

    wordModel = mapBase.getNextWordInfo();
    assert.ok(wordModel instanceof ShowHideWordModel, 'getNextWordInfo second call  return ShowHideWordModel');
    assert.ok(wordModel.getId() === 'right', 'getNextWordInfo() getId');
    assert.ok(wordModel.getWordText() === 'Right 2', 'getNextWordInfo() getName');
    assert.ok(wordModel.getDemonId() === null, 'getNextWordInfo() getDemonId');

    assert.ok(mapBase.onCallbackTileDrawSet instanceof Function, 'onCallbackTileDrawSet is function');
    assert.ok(mapBase.cbCheckRightAnswer instanceof Function, 'cbCheckRightAnswer is function');

    assert.throws(
        function() {
            mapBase.onCallbackTileDrawSet();
        },
        new Error('onCallbackTileDrawSet test'),
        "raised error onCallbackTileDrawSet test"
    );

    assert.throws(
        function() {
            mapBase.cbCheckRightAnswer();
        },
        new Error('cbCheckRightAnswer test'),
        "raised error cbCheckRightAnswer test"
    );
});

//
// QUnit.test('ShowHide.Demon.OuroborosRes', function (assert) {
//     var bodyImg = new HTMLImageElement();
//     var lightImg = new HTMLImageElement();
//     var topImg = new HTMLImageElement();
//     var model = new ShowHide.Demon.OuroborosRes({
//         'body' : { 'img': new bodyImg},
//         'light' : { 'img': new lightImg},
//         'top' : { 'img': new topImg}
//     });
//
//     model.getBodyImg() === bodyImg
//     model.getLightImg() === bodyImg
//     model.getParasiteImg() === bodyImg
//
// });