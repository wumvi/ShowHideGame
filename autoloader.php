<?php

spl_autoload_register(function ($className) {
    $classNamePath = str_replace('\\', '/', $className);
    $classFileName = CORE_ROOT . $classNamePath . '.php';
    // echo $classFileName, '<br/>', PHP_EOL;
    return include($classFileName);
});
